using System;
using Robocode;
using Robocode.Util;

namespace ALX
{

    class Alex01 : AdvancedRobot
    {
        private int count;
        private double gunTurnAmt;
        private String trackName;
        public override void Run()
        {

            
            trackName = null;
            gunTurnAmt = 11;
            IsAdjustGunForRobotTurn = true;

            while (true)
            {

                Movimentacao();

                count++;
                
                if (count > 2)
                {
                    gunTurnAmt = -11;
                }
                
                if (count > 5)
                {
                    gunTurnAmt = 11;
                }
                
                if (count > 11)
                {
                    trackName = null;
                }



            }
        }
        public override void OnScannedRobot(ScannedRobotEvent evnt)
        {

            if (trackName != null && evnt.Name != trackName)
            {
                return;
            }

            if (trackName == null)
            {
                trackName = evnt.Name;
            }

            gunTurnAmt = Utils.NormalRelativeAngleDegrees(evnt.Bearing + (Heading - RadarHeading));
            Movimentacao();
            Fire(5);

        }

        public void Movimentacao()
        {
            TurnGunRight(gunTurnAmt);
            SetAhead(1000);
            SetTurnLeft(90);
        }

    }
}