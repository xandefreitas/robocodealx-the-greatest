O robo ALX inicia movimentando-se para frente e para a esquerda de forma circular, enquanto isso, a arma e o scanner rodam independentes a procura de um oponente. Ao Encontrar o oponente, inicia-se os disparos com a mira fixada no alvo mas sem parar a movimentação do corpo, permitindo assim uma alta taxa de acertos e desvios de tiros inimigos.

Foi uma jornada árdua de muita leitura, pesquisa e testes para criar um robo que se destacasse dentre os exemplos ja criados no robocode, fiquei intrigado por todo o processo e perdi noites querendo sempre fazer uma melhoria no meu robo ou compara-lo com outros oponentes.

Agradeço muito a oportunidade de fazer parte de um processo seletivo diferente e que me fez estudar tanto!